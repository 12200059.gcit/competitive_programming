def clearBit(n, k):
    return (n & ( ~(1 << (k - 1))))
 
# Driver code
n = 5
k = 1
 
print(clearBit(n, k))