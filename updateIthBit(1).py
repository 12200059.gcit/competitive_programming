# Online Python compiler (interpreter) to run Python online.
def updateIthBit(num,i,value):
    mask1=1<<i
    result=num & ~mask1
    mask2=value<<i
    result= result | mask2
    return result

print(updateIthBit(13,1,1))
# check
print(bin(15))
print(updateIthBit(13,2,0))
# check
print(bin(9))