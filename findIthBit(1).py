def findIthBit(num,i):
    mask=1<<i
    num=num&mask
    if(num>0):
        return 1
    else:
        return 0

print(findIthBit(13,1))
print(findIthBit(13,2))