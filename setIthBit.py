# Online Python compiler (interpreter) to run Python online.
def findIthBit(num,i):
    mask=1<<i
    num=num | mask
    return num

print(findIthBit(13,1))
# check
print(bin(15))
print(findIthBit(13,2))
# check
print(bin(13))