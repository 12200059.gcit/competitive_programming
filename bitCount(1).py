def countBits(n):
  
    count = 0
    while (n):
        count += 1
        n >>= 1
          
    return count
  
# Driver program
i = 65
print(countBits(i))