# def clear_i(num, i):
#     mask = 1 << i
#     mask = -mask
#     num = num & mask
#     return num

# print(clear_i(13,2))
def clearBit(n, k):
    return (n & ( ~(1 << (k - 1))))
 
# Driver code
n = 13
k = 2
 
print(clearBit(n, k))