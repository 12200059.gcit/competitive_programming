def uniquenumber(x):
    ans = 0
    for num in x:
        ans = ans ^ num
    return ans

list1 = [-1 ,0 ,1 ,9 ,-2 ,11 ,0 ,1 ,-2 ,9 ,11 ]
list2 = [-1 ,0 ,1 ,9 ,-2 ,11 ,-1 ,1 ,-2 ,9 ,11 ]
list3 = [-1 ,0 ,1 ,9 ,-2 ,11 ,0 ,-1 ,-2 ,1 ,11 ]
print(uniquenumber(list1))
print(uniquenumber(list2))
print(uniquenumber(list3))